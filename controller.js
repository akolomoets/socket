var MessageModel = require("./db/schema").MessageSchema;
var RoomModel = require("./db/schema").RoomSchema;

function createMessage(props, author) {
    var message = new MessageModel();
    var currentDate = new Date();
    message.author = author;
    message.created = currentDate.getFullYear() + "-" + currentDate.getMonth() + "-" + currentDate.getDate();
    message.text = props.text;
    message.room = props.room;

    message.save(function (err) {
        if (!err) {
            return console.log({status: "OK"});
        } else {
            if (err.name == "ValidationError") {
                console.log({error: "Validation error"});
            } else {
                console.log({error: "Server error"});
            }
        }
    });
}

function createRoom(name) {
    var currentDate = new Date();
    var newRoom = new RoomModel();
    newRoom.name = name;
    newRoom.created = currentDate.getFullYear() + "-" + currentDate.getMonth() + "-" + currentDate.getDate();

    return RoomModel.findOne({'name': newRoom.name}, function (err, room) {
        if (room) {
            return console.log('room ', name, ' have already exists')
        } else {
            newRoom.save(function (err) {
                if (!err) {
                    return console.log({status: "OK"});
                } else {
                    if (err.name == "ValidationError") {
                        console.log({error: "Validation error"});
                    } else {
                        console.log({error: "Server error"});
                    }
                    return console.log({status: "err"});
                }
            });
        }
    });
}

function getRoomHistory(room) {
    return MessageModel.find({"room": room}, function (err, messages) {
        if (!err) {
            return messages;
        } else {
            console.log({error: "Server error"});
        }
    }).sort({created: 1}).limit(15);
}

function getRooms() {
    return RoomModel.find({}, function (err, rooms) {
        if (!err) {
            return rooms;
        } else {
            console.log({error: "Server error"});
        }
    });
}

module.exports = {
    createMessage: createMessage,
    createRoom: createRoom,
    getRoomHistory: getRoomHistory,
    getRooms: getRooms
}