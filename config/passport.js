var LocalStrategy = require('passport-local').Strategy,
    UserModel = require("../db/schema").UserSchema;

module.exports = function (passport) {
    passport.serializeUser(function (user, done) {
        done(null, user.email);
    });

    passport.deserializeUser(function (email, done) {
        UserModel.findOne({"email": email}, function (err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {
            console.log('passport', email, password);
            process.nextTick(function() {
                UserModel.findOne({'email': email}, function (err, user) {
                    if (err) {
                        return done(err);
                    }
                    if (user) {
                        return done(null, false);
                    } else {
                        var newUser = new UserModel();
                        newUser.email = email;
                        newUser.password = newUser.generateHash(password);

                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                })
            })
        })
    );

    passport.use('local-login', new LocalStrategy({
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true
        },
        function(req, email, password, done) {
            UserModel.findOne({ 'email' :  email }, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false);
                }
                if (!user.validPassword(password, user.password)) {
                    return done(null, false);
                }
                return done(null, user);
            });

        })
    );
};
