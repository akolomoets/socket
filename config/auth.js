function checkAuth(req, res, next){
    if (req.isAuthenticated()){
        next(null);
    } else {
        res.redirect('/')
    }
}

module.exports = {
    checkAuth: checkAuth
}
