var cors = require('express-cors');

function corsConfig (app) {
    return app.use(cors({
        allowedOrigins: [
            'http://localhost:3000',
            'http://127.0.0.1:3000'
        ],
        methods: [
          'GET', 'POST', 'PUT', 'DELETE'
        ]
    }));
}

module.exports = {
    corsConfig: corsConfig
}
