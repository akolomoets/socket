var express = require('express');
var expressLayouts = require('express-ejs-layouts');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require("path");
var fs = require("fs");
var bodyParser = require("body-parser");
var passport = require('passport');
var flash = require('connect-flash');
var session = require('express-session');

require('./config/passport')(passport);

app.use(bodyParser.json());
app.use(require('cookie-parser')());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));

app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'ejs');
app.use(expressLayouts);

app.use(express.static(__dirname + '/public'));
require('./app/router')(app, passport, path);

require('./chatServer')(io, path, fs);

http.listen(3000, function () {
    console.log('listening on *:3000');
});
