function renderRooms(rooms) {
    $.each(rooms, function (index, room) {
        if (room.name !== '') {
            var rout = '/room/' + room.name;
            var item = $('<a></a>').text(room.name);
            item.addClass('list-group-item');
            item.attr('href', rout);
            $('#room-list').append(item);
        }
    });
}

$(document).ready(function () {
    $.get('rooms', function (rooms) {
        $('#room-list').empty();
        renderRooms(rooms);
    });

    $('#room-button').click(function () {
        var name = $('#room-input').val();
        $.post('newRoom', {room: name});
        var rooms = [{name: name}];
        renderRooms(rooms);
    });
});