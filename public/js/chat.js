var Chat = function (socket) {
    this.socket = socket;
}

Chat.prototype.sendMessage = function (room, text) {
    var message = {
        room: room,
        text: text
    };
    this.socket.emit('message', message);
};

Chat.prototype.sendFile = function (e, room) {
    var file = $('#file')[0];
    if (!file.files.length) {
        return;
    }

    var firstFile = file.files[0],
        reader = new FileReader();

    $('#messages').append(divEscapedContentElement('you: '));
    if (firstFile.type.indexOf("image") !== -1) {
        $('#messages').append(divEscapedContentElementFile(firstFile.name));
    } else {
        $('#messages').append(divEscapedContentElement(firstFile.name))
    }

    reader.onloadend = function () {
        socket.emit('file', {
            name: firstFile.name,
            data: reader.result,
            room: room,
            type: firstFile.type
        });
    };

    reader.readAsArrayBuffer(firstFile);
};

function divEscapedContentElementFile(fileName) {
    var img = $('<img/>');
    img.attr('src', '/tmp/' + fileName);
    img.attr('width', '270px');
    return img;
}
function divEscapedContentElement(msg) {
    return $('<div></div>').text(msg);
}
function divSystemContentElement(msg) {
    return $('<div></div>').html('<i>' + msg + '</i>');
}

function processUserInput(chatApp, socket) {
    var message = $('#send-message').val();
    chatApp.sendMessage($('#room').text(), message);
    $('#messages').append(divEscapedContentElement('you: ' + message));
    $('#messages').scrollTop($('#messages').prop('scrollHeight'));
    $('#send-message').val('');
}

var socket = io.connect();

$(document).ready(function () {
    var currentRoom = window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1);
    $('#room').text(currentRoom);

    var chatApp = new Chat(socket);
    socket.emit('join', currentRoom);

    socket.on('nameResult', function (result) {
        var message;

        if (result.success) {
            message = 'You are' + result.name;
        } else {
            message = result.message;
        }
        $('#messages').append(divSystemContentElement(message));
    });

    socket.on('message', function (message) {
        var newElement = $('<div></div>').text(message.text);
        $('#messages').append(newElement);
    });

    socket.on('file-uploaded', function (message) {
        var src = message.name;
        if (message.type.indexOf('image') !== -1) {
            var img = document.createElement('img');
            img.setAttribute('src', src);
            img.setAttribute('width', '270px');
            $('#messages').append($('<div></div>').text(message.text));
            $('#messages').append(img);
        } else {
            var a = document.createElement('a')
            a.setAttribute('href', src);
            a.setAttribute('download', 'download');
            a.innerHTML = message.name.slice(message.name.lastIndexOf('/') + 1);
            $('#messages').append($('<div></div>').text(message.text));
            $('#messages').append(a);
        }
    });

    socket.on('showHistory', function (history) {
        $.each(history, function (index, message) {
            var msg = $('<div></div>').text(message.author + ': ' + message.text);
            $('#messages').append(msg);
        });
    });

    $('#send-message').focus();

    $('#file').change(function (e) {
        chatApp.sendFile(e, currentRoom);
    });
    $('#send-button').click(function () {
        processUserInput(chatApp, socket);
        return false;
    });
});