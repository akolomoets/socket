var mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs'),
    config =  require('../config/main');

mongoose.Promise = global.Promise;

mongoose.connect(config.db);
var db = mongoose.connection;

db.on('error', function (err) {
    console.log(err);
});

db.once('open', function callback () {
    console.log("Connected to DB!");
});

module.exports = {
    mongoose: mongoose
}
