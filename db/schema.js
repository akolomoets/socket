var mongoose = require('./mongoose').mongoose,
    bcrypt = require('bcrypt-nodejs'),
    UserSchema = new mongoose.Schema({
        email: String,
        password: String,
        username: String
    }),
    RoomSchema = new mongoose.Schema({
        name: String,
        created: String,
        messages: Boolean
    }),
    MessageSchema = new mongoose.Schema({
        author: String,
        created: String,
        text: String,
        room:  String
});

UserSchema.methods.validPassword = function (password, originalPass) {
    return bcrypt.compareSync(password, originalPass);
};

UserSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

module.exports = {
    UserSchema: mongoose.model('chat_user', UserSchema),
    RoomSchema: mongoose.model('chat_room', RoomSchema),
    MessageSchema: mongoose.model('chat_message', MessageSchema),
}
