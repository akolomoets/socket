var guestNumber = 1;
var nickNames = {};
var namesUsed = [];
var currentRoom = {};
var controller = require('./controller')

function assignGuestName(socket, guestNumber, nickNames, namesUsed) {
    var name = 'Guest' + guestNumber;
    nickNames[socket.id] = name;
    socket.emit('nameResult', {
        success: true,
        name: name
    })
    namesUsed.push(name);
    return guestNumber + 1;
}

function joinRoom(socket, room) {
    socket.join(room);
    currentRoom[socket.id] = room;
    socket.emit('joinResult', {room: room});
    socket.to(room).emit('message', {
        text: nickNames[socket.id] + ' has joined ' + room + '.'
    });
}

function handleMessageBroadcasting(socket, nickNames) {
    socket.on('message', function (message) {
        controller.createMessage(message, nickNames[socket.id])
        socket.to(message.room).emit('message', {
            text: nickNames[socket.id] + ': ' + message.text
        });
    });
}

function handleFileBroadcasting(socket, fs, path) {
    socket.on('file', function (message) {
        var writer = fs.createWriteStream(path.resolve(__dirname, './public/tmp/' + message.name), {encoding: 'base64'});
        writer.write(message.data);
        writer.end();
        writer.on('finish', function () {
            socket.to(message.room).emit('file-uploaded', {name: '/tmp/' + message.name, type: message.type, text: nickNames[socket.id] + ': '});
        });
    });
};

function handleRoomJoining(socket) {
    socket.on('join', function (room) {

        console.log('join', room);

        socket.leave(currentRoom[socket.id]);
        joinRoom(socket, room);

        controller.createRoom(room);

        controller.getRoomHistory(room)
            .then(function (res) {
                socket.emit('showHistory', res);
            })
    });
}

module.exports = function (io, path, fs) {
    io.on('connection', function (socket) {
        console.log('a user connected');
        guestNumber = assignGuestName(socket, guestNumber, nickNames, namesUsed);
        handleMessageBroadcasting(socket, nickNames);
        handleFileBroadcasting(socket, fs, path);
        handleRoomJoining(socket);

        controller.getRooms()
            .then(function (res) {
                socket.emit('renderRooms', res);
            })

        socket.on('rooms', function () {
            socket.emit('rooms', socket.rooms);
        });

        socket.on('disconnect', function () {
            console.log('user disconnected');
            var nameIndex = namesUsed.indexOf(nickNames[socket.id]);
            delete namesUsed[nameIndex];
            delete nickNames[socket.id];
        });
    });
}
