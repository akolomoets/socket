var controller = require('./../controller');
var checkAuth = require('./../config/auth').checkAuth;

module.exports = function (app, passport, path) {

    app.get('/', function (req, res) {
        res.render('pages/home');
    });

    app.get('/login', function (req, res) {
        res.render('pages/login');
    });

    app.get('/register', function (req, res) {
        res.render('pages/register');
    });

    app.get('/room/:roomName', checkAuth, function (req, res) {
        res.render('pages/room');
    });

    app.get('/rooms', checkAuth, function (req, res) {
        controller.getRooms()
            .then(function(rooms){
                res.send(rooms);
            })
    });

    app.get('/lobby',checkAuth, function (req, res) {
        res.render('pages/lobby');
    });

    app.get('/*', function (req, res) {
        res.redirect('/');
    });

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/lobby', // redirect to the secure profile section
        failureRedirect : '/' // redirect back to the signup page if there is an error
    }));

    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/lobby', // redirect to the secure profile section
        failureRedirect : '/' // redirect back to the signup page if there is an error
    }));

    app.post('/newRoom', function (req, res) {
        controller.createRoom(req.body.room)
            .then(function(){
                res.send("OK");
            })
    });
}
